package be.ucll.java.ent.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Enumeration;

/**
 * The Class HttpServletRequestDebug is an Utility class to print all the
 * informations of a {@link HttpServletRequest} and its session and cookies.
 * Thefunctionality is wrapped with a trycatch to avoid any side-effect.
 */
public class HttpServletRequestDebug {

    public static void debugRequest(final HttpServletRequest httpServletRequest) {
        try {
            printRequest(httpServletRequest);
        } catch (final Throwable e) {
            System.err.println("Could not dump the servlet");
        }
    }

    /**
     * Prints the request.
     *
     * @param httpServletRequest the http servlet request
     */
    private static void printRequest(final HttpServletRequest httpServletRequest) {
        if (httpServletRequest == null) {
            return;
        }
        System.out.println("----------------------------------------");
        System.out.println("W4 HttpServletRequest");
        System.out.println("\tRequestURL: " + httpServletRequest.getRequestURL());
        System.out.println("\tRequestURI: " + httpServletRequest.getRequestURI());
        System.out.println("\tScheme: " + httpServletRequest.getScheme());
        System.out.println("\tAuthType: " + httpServletRequest.getAuthType());
        System.out.println("\tEncoding: " + httpServletRequest.getCharacterEncoding());
        System.out.println("\tContentLength: " + httpServletRequest.getContentLength());
        System.out.println("\tContentType: " + httpServletRequest.getContentType());
        System.out.println("\tContextPath: " + httpServletRequest.getContextPath());
        System.out.println("\tMethod: " + httpServletRequest.getMethod());
        System.out.println("\tPathInfo: " + httpServletRequest.getPathInfo());
        System.out.println("\tProtocol: " + httpServletRequest.getProtocol());
        System.out.println("\tQuery: " + httpServletRequest.getQueryString());
        System.out.println("\tRemoteAddr: " + httpServletRequest.getRemoteAddr());
        System.out.println("\tRemoteHost: " + httpServletRequest.getRemoteHost());
        System.out.println("\tRemotePort: " + httpServletRequest.getRemotePort());
        System.out.println("\tRemoteUser: " + httpServletRequest.getRemoteUser());
        System.out.println("\tSessionID: " + httpServletRequest.getRequestedSessionId());
        System.out.println("\tServerName: " + httpServletRequest.getServerName());
        System.out.println("\tServerPort: " + httpServletRequest.getServerPort());
        System.out.println("\tServletPath: " + httpServletRequest.getServletPath());

        System.out.println("");
        System.out.println("\tCookies");
        int i = 0;
        for (final Cookie cookie : httpServletRequest.getCookies()) {
            System.out.println("\tCookie[" + i + "].name: " + cookie.getName());
            System.out.println("\tCookie[" + i + "].comment: " + cookie.getComment());
            System.out.println("\tCookie[" + i + "].domain: " + cookie.getDomain());
            System.out.println("\tCookie[" + i + "].maxAge: " + cookie.getMaxAge());
            System.out.println("\tCookie[" + i + "].path: " + cookie.getPath());
            System.out.println("\tCookie[" + i + "].secured: " + cookie.getSecure());
            System.out.println("\tCookie[" + i + "].value: " + cookie.getValue());
            System.out.println("\tCookie[" + i + "].version: " + cookie.getVersion());
            i++;
        }
        System.out.println("\tDispatcherType: " + httpServletRequest.getDispatcherType());
        System.out.println("");

        System.out.println("\tHeaders");
        int j = 0;
        final Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            final String headerName = headerNames.nextElement();
            final String header = httpServletRequest.getHeader(headerName);
            System.out.println("\tHeader[" + j + "].name: " + headerName);
            System.out.println("\tHeader[" + j + "].value: " + header);
            j++;
        }

        System.out.println("\tLocalAddr: " + httpServletRequest.getLocalAddr());
        System.out.println("\tLocale: " + httpServletRequest.getLocale());
        System.out.println("\tLocalPort: " + httpServletRequest.getLocalPort());

        System.out.println("");
        System.out.println("\tParameters");
        int k = 0;
        final Enumeration<String> parameterNames = httpServletRequest.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            final String paramName = parameterNames.nextElement();
            final String paramValue = httpServletRequest.getParameter(paramName);
            System.out.println("\tParam[" + k + "].name: " + paramName);
            System.out.println("\tParam[" + k + "].value: " + paramValue);
            k++;
        }

        System.out.println("");
        System.out.println("\tParts");
        int l = 0;
        try {
            for (final Object part : httpServletRequest.getParts()) {
                if (part != null) {
                    System.out.println("\tParts[" + l + "].class: " + part.getClass());
                    System.out.println("\tParts[" + l + "].value: " + part.toString());
                }
                l++;
            }
        } catch (final Exception e) {
            System.err.println("Exception " + e);
        }
        printSession(httpServletRequest.getSession());
        printUser(httpServletRequest.getUserPrincipal());

		/*
		try {
			System.out.println("Request Body: " +
					IOUtils.toString(httpServletRequest.getInputStream(), httpServletRequest.getCharacterEncoding()));
			System.out.println("Request Object: " + new ObjectInputStream(httpServletRequest.getInputStream()).readObject());
		} catch (final Exception e) {
			// ignore
		}
		*/
        System.out.println("----------------------------------------");
    }

    /**
     * Prints the session.
     *
     * @param session the session
     */
    private static void printSession(final HttpSession session) {
        System.out.println("-");
        if (session == null) {
            System.err.println("No session");
            return;
        }
        System.out.println("\tSession Attributes");
        System.out.println("\tSession.id: " + session.getId());
        System.out.println("\tSession.creationTime: " + session.getCreationTime());
        System.out.println("\tSession.lastAccessTime: " + session.getLastAccessedTime());
        System.out.println("\tSession.maxInactiveInterval: " + session.getMaxInactiveInterval());

        int k = 0;
        final Enumeration<String> attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            final String paramName = attributeNames.nextElement();
            final Object paramValue = session.getAttribute(paramName);
            System.out.println("\tSession Attribute[" + k + "].name: " + paramName);
            if (paramValue.getClass() != null) {
                System.out.println("\tSession Attribute[" + k + "].class: " + paramValue.getClass());
            }
            System.out.println("\tSession Attribute[" + k + "].value: " + paramValue);
            k++;
        }

    }

    /**
     * Prints the user.
     *
     * @param userPrincipal the user principal
     */
    private static void printUser(final Principal userPrincipal) {
        System.out.println("-");
        if (userPrincipal == null) {
            System.out.println("User Authentication : none");
            return;
        } else {
            System.out.println("User Authentication.name : " + userPrincipal.getName());
            System.out.println("User Authentication.class : " + userPrincipal.getClass());
            System.out.println("User Authentication.value : " + userPrincipal);
        }

    }

}
