package be.ucll.java.ent.model;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Student")
@NamedQueries({
    @NamedQuery(name = "Student.getAll", query = "SELECT e FROM StudentEntity e"),
    @NamedQuery(name = "Student.countAll", query = "SELECT count(e) FROM StudentEntity e")
})
public class StudentEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sseq")
    @SequenceGenerator(name = "sseq", sequenceName = "student_sequence", allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private long id;

    @Column
    private String naam;


    @Column
    private String voornaam;

    @Column
    private Date geboortedatum;

    // Constructors
    public StudentEntity() {
        // Default constructor
    }

    public StudentEntity(long id) {
        this.id = id;
    }

    public StudentEntity(long id, String naam) {
        this.id = id;
        this.naam = naam;

    }
    public StudentEntity(long id, String naam, String voornaam) {
        this.id = id;
        this.naam = naam;
        this.voornaam = voornaam;
    }

    public StudentEntity(long id, String naam, String voornaam, Date geboortedatum) {
        this.id = id;
        this.naam = naam;
        this.voornaam = voornaam;
        this.geboortedatum = geboortedatum;
    }

    // Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public Date getGeboortedatum() {
        return geboortedatum;
    }

    public void setGeboortedatum(Date geboortedatum) {
        this.geboortedatum = geboortedatum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentEntity student = (StudentEntity) o;
        return id == student.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, naam);
    }

    @Override
    public String toString() {
        return "StudentEntity{" +
                "id=" + id +
                ", naam='" + naam + '\'' +
                '}';
    }
}